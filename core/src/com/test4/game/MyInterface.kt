package com.test4.game

interface MyInterface {
    val isSignedIn : Boolean
    fun signIn()
    fun signOut()
}