package com.test4.game

import com.badlogic.gdx.Application
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.utils.Logger
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.utils.viewport.Viewport

class Main(var myInterface: MyInterface?) : ApplicationAdapter() {
    companion object {
        private val log = Logger(Main::class.java.simpleName, Logger.ERROR)
    }

    private lateinit var batch: PolygonSpriteBatch
    private lateinit var camera: OrthographicCamera
    private lateinit var viewport: Viewport
    private lateinit var skin: Skin
    private lateinit var stage: Stage

    lateinit var button: TextButton

    override fun create() {

        Gdx.app.logLevel = Application.LOG_ERROR

        camera = OrthographicCamera()
        viewport = FitViewport(800f, 480f, camera)
        batch = PolygonSpriteBatch()

        stage = Stage()
        skin = Skin(Gdx.files.internal("uiskin.json"))

        createUI()

        Gdx.input.inputProcessor = stage

    }

    private fun createUI() {
        button = TextButton("Sign In", skin)
        button.setPosition(300f, 100f)
        button.setSize(200f, 200f)
        button.addListener(object : ChangeListener() {
            override fun changed(event: ChangeListener.ChangeEvent, actor: Actor) {
                if (myInterface != null) {
                    if (myInterface!!.isSignedIn) {
                        myInterface!!.signOut()
                        button.setText("Sign In")
                    } else {
                        myInterface!!.signIn()
                        button.setText("Sign Out")
                    }
                }
            }
        })
        stage.addActor(button)
    }


    override fun render() {
        /* This makes sure screen in create() renders */
        super.render()

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        batch.projectionMatrix = camera.combined
        batch.begin()
        batch.end()

        stage.act()
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        viewport.update(width, height)
    }

    /** Dispose all disposables and static reference with states */
    override fun dispose() {
        log.error("dispose")
        super.dispose() //disposes current screen
        myInterface!!.signOut() //signs out every time
    }

    override fun resume() {
        super.resume()
        log.error("resume")
    }

    override fun pause() {
        super.pause()
        log.error("pause")
    }

}